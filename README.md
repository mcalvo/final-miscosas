# final-miscosas

Repositorio de inicio de la práctica final. Recuerda realizar una derivación (fork) de este repositorio y hacer múltiples commits al realizar tu práctica.

# Entrega practica

## Datos

* Nombre: Manuela Calvo Barrios
* Titulación: Doble grado en ingeniería en sistemas de telecomunicación y administración y dirección de empresas
* Despliegue (url): http://mcalvo.pythonanywhere.com/
* Vídeo básico (url): https://www.youtube.com/watch?v=yjE-bMZQerc
* Vídeo parte opcional (url): https://www.youtube.com/watch?v=kPiJ6d-wVag

* Cuenta GITLAB ETSIT y LABS ETSIT: mcalvo

## Cuenta Admin Site

* mcalvo/admin

## Cuentas usuarios

* manuela/123
* pepe/123
* dani/123

## Resumen parte obligatoria

Para mostrar los botones de voto de los items resaltados se han utilizado filtros en las plantillas. Los filtros empleados se encuentran implementados en el fichero filters.py.

## Lista partes opcionales

* Favicon
* Canales RSS en XML y JSON: opción de descarga y vista en web
* Postear imágenes en los comentarios
* Foto de perfil por defecto en los usuarios que no tienen
* Canales RSS de comentarios de los items
* Internacionalización con i18n de django
* Ampliación de test a las vistas de la parte opcional
