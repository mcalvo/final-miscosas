from django.contrib import admin

from .models import Video, Comentario, Voto, Alimentador, Noticia, Usuario, Item, Estilo

# Register your models here.

admin.site.register(Video)
admin.site.register(Alimentador)
admin.site.register(Voto)
admin.site.register(Comentario)
admin.site.register(Noticia)
admin.site.register(Usuario)
admin.site.register(Item)
admin.site.register(Estilo)
