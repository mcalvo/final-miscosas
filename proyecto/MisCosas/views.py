from django.contrib.auth import logout, authenticate, login
from django.contrib.auth.models import User
from django.db import IntegrityError
from django.db.models import Count
from django.shortcuts import get_object_or_404, redirect, render, HttpResponse
from django.views.decorators.csrf import csrf_exempt
from xml.sax.handler import ContentHandler
from .models import Video, Voto, Comentario, Alimentador, Noticia, Item, Usuario, Estilo
from .apps import xmlStream
from .forms import ComentarioForm, FotoUsuarioForm, EstiloForm, YoutubeForm, SubredditForm
from . import ytparser
from. import rdparser

import mimetypes
import urllib.request
import sys
import operator



def get_estilo(request):
    if request.user.is_authenticated:
        try:
            try:
                u = Usuario.objects.get(usuario=request.user)
                estiloId = Estilo.objects.get(usuario=u).estiloId
            except Usuario.DoesNotExist: # el admin no está en la base de datos de Usuarios
                estiloId = 'sinEstilo'
        except Estilo.DoesNotExist: # si el usuario no tiene estilo predefinido
            estiloId = 'sinEstilo'
    else: # si no hay usuario autenticado
        estiloId = 'sinEstilo'
    return estiloId



# Create your views here.

@csrf_exempt
def login_view(request):
    if request.method == 'POST':
        username = request.POST['user']
        password = request.POST['password']
        user = authenticate(request, username=username, password=password)
        if user is not None:
            login(request, user)
        return redirect(request.POST['origen'])
    else:
        return render(request, 'MisCosas/invalid.html', {}, status=404)



@csrf_exempt
def logout_view(request):
    logout(request)
    return redirect(request.POST['origen'])



@csrf_exempt
def registro(request):
    if request.method == "GET":
        context = {'registrado': False}
        return render(request, 'MisCosas/form_registro.html', context)
    elif request.method == "POST":
        username = request.POST['usuario']
        password = request.POST['contraseña']
        email = request.POST['email']
        try:
            user = User.objects.create_user(username, email, password)
            usuario = Usuario(usuario=user, url='http://localhost:8000/MisCosas/usuario/' + username)
            if request.FILES:
                usuario.foto = request.FILES['foto']
            usuario.save()
            context = {'registrado': True}
        except IntegrityError:
            context = {'registrado': False, 'error': True}
        return render(request, 'MisCosas/form_registro.html', context)
    else:
        return render(request, 'MisCosas/invalid.html', {}, status=404)



def index(request):
    if request.method == 'GET':
        items_diez = Item.objects.annotate(punt=Count('puntuacion')).order_by('-puntuacion')[:10]
        items_cinco = Voto.objects.filter(usuario=request.user.username).annotate(f=Count('fecha')).order_by('-fecha')[:5]
        alimentadores = Alimentador.objects.filter(seleccionado=True)
        estiloId = get_estilo(request)
        context = {'alimentadores': alimentadores, 'items_diez': items_diez, 'items_cinco': items_cinco, 'estiloId': estiloId,
                    'formYt': YoutubeForm, 'formSd': SubredditForm}
        return render(request, 'MisCosas/indexApp.html', context, status=200)
    else:
        return render(request, idioma + 'MisCosas/invalid.html', {}, status=404)



def xml(request):
    if request.method == 'GET':
        items_diez = Item.objects.annotate(punt=Count('puntuacion')).order_by('-puntuacion')[:10]
        items_cinco = Voto.objects.filter(usuario=request.user.username).annotate(f=Count('fecha')).order_by('-fecha')[:5]
        alimentadores = Alimentador.objects.all()
        context = {'alimentadores': alimentadores, 'items_diez': items_diez, 'items_cinco': items_cinco, 'user': request.user}
        return render(request, 'MisCosas/indexApp.xml', context, content_type='text/xml')
    else:
        return render(request, 'MisCosas/invalid.html', {}, status=404)


def json(request):
    if request.method == 'GET':
        items_diez = Item.objects.annotate(punt=Count('puntuacion')).order_by('-puntuacion')[:10]
        items_cinco = Voto.objects.filter(usuario=request.user.username).annotate(f=Count('fecha')).order_by('-fecha')[:5]
        alimentadores = Alimentador.objects.filter(seleccionado=True)
        context = {'alimentadores': alimentadores, 'items_diez': items_diez, 'items_cinco': items_cinco, 'user': request.user}
        return render(request, 'MisCosas/indexApp.json', context, content_type='application/json')
    else:
        return render(request, 'MisCosas/invalid.html', {}, status=404)


def descarga(request, doc):
    if request.method == 'GET':
        fl_path = 'MisCosas/' + doc
        filename = doc
        mime_type, _ = mimetypes.guess_type(fl_path)
        if doc == 'indexApp.xml' or doc == 'indexApp.json':
            items_diez =  Item.objects.annotate(punt=Count('puntuacion')).order_by('-puntuacion')[:10]
            items_cinco = Voto.objects.filter(usuario=request.user.username).annotate(f=Count('fecha')).order_by('-fecha')[:5]
            alimentadores = Alimentador.objects.filter(seleccionado=True)
            context = {'alimentadores': alimentadores, 'items_diez': items_diez, 'items_cinco': items_cinco, 'user': request.user}
        elif doc == 'indexAlimentadores.xml' or doc == 'indexAlimentadores.json':
            alimentadores = Alimentador.objects.all()
            context = {'feeders': alimentadores}
        elif doc == 'usuarios.xml' or doc == 'usuarios.json':
            usuarios = Usuario.objects.all()
            context = {'users': usuarios}
        response = render(request, fl_path, context, content_type=mime_type)
        response['Content-Disposition'] = "attachment; filename=%s" % filename
        return response
    else:
        return render(request, 'MisCosas/invalid.html', {}, status=404)



def descarga_id(request, doc, id):
    if request.method == 'GET':
        fl_path = 'MisCosas/' + doc
        filename = doc
        mime_type, _ = mimetypes.guess_type(fl_path)
        if doc == 'youtube.xml' or doc == 'youtube.json':
            items = Video.objects.filter(alimentador=Alimentador.objects.get(alimentadorId=id))
            context = {'items': items}
        elif doc == 'subreddit.xml' or doc == 'subreddit.json':
            items = Noticia.objects.filter(alimentador=Alimentador.objects.get(alimentadorId=id))
            context = {'items': items}
        else:
            return render(request, 'MisCosas/invalid.html', {}, status=404)
        response = render(request, fl_path, context, content_type=mime_type)
        response['Content-Disposition'] = "attachment; filename=%s" % filename
        return response
    else:
        return render(request, 'MisCosas/invalid.html', {}, status=404)



def descarga_com(request, id, doc):
    if request.method == 'GET':
        fl_path = 'MisCosas/' + doc
        filename = doc
        mime_type, _ = mimetypes.guess_type(fl_path)
        try:
            comentarios = Comentario.objects.filter(video=Video.objects.get(videoId=id))
            item = Video.objects.get(videoId=id)
        except Video.DoesNotExist:
            comentarios = Comentario.objects.filter(noticia=Noticia.objects.get(noticiaId=id))
            item = Noticia.objects.get(noticiaId=id)
        context = {'comments': comentarios, 'item': item}
        response = render(request, fl_path, context, content_type=mime_type)
        response['Content-Disposition'] = "attachment; filename=%s" % filename
        return response
    else:
        return render(request, 'MisCosas/invalid.html', {}, status=404)




def alimentadores(request):
    if request.method == 'GET':
        alimentadores = Alimentador.objects.all()
        estiloId = get_estilo(request)
        context = {'alimentadores': alimentadores, 'estiloId': estiloId}
        return render(request, 'MisCosas/indexAlimentadores.html', context)
    else:
        return render(request, 'MisCosas/invalid.html', {}, status=404)



def xml_alimentadores(request):
    if request.method == 'GET':
        alimentadores = Alimentador.objects.all()
        context = {'feeders': alimentadores}
        return render(request, 'MisCosas/indexAlimentadores.xml', context, content_type='text/xml')
    else:
        return render(request, 'MisCosas/invalid.html', {}, status=404)



def json_alimentadores(request):
    if request.method == 'GET':
        alimentadores = Alimentador.objects.all()
        context = {'feeders': alimentadores}
        return render(request, 'MisCosas/indexAlimentadores.json', context, content_type='application/json')
    else:
        return render(request, 'MisCosas/invalid.html', {}, status=404)



def eliminar(request, id):
    if request.method == 'POST':
        try:
            a = Alimentador.objects.get(alimentadorId=id)
            a.seleccionado = False
            a.save()
            if request.POST['origen'] == 'index':
                return redirect('index')
            else:
                return redirect(request.POST['origen'], id)
        except Alimentador.DoesNotExist:
            context = {'recurso': 'Alimentador'}
            return render(request, 'MisCosas/error.html', context, status=404)
    else:
        return render(request, 'MisCosas/invalid.html', {}, status=404)



def seleccionar(request, id):
    if request.method == 'POST':
        try:
            a = Alimentador.objects.get(alimentadorId=id)
            if a.nombre == 'youtube':
                url = 'https://www.youtube.com/feeds/videos.xml?channel_id=' + id
                xmlStream = urllib.request.urlopen(url)
                ytparser.Parser.parse(xmlStream)
            else:
                rdId = id + '.rss'
                url = 'https://www.reddit.com/r/' + rdId
                xmlStream = urllib.request.urlopen(url)
                rdparser.Parser.parse(xmlStream)
            a.seleccionado = True
            a.save()
            return redirect(a.nombre, id=a.alimentadorId)
        except Alimentador.DoesNotExist:
            context = {'recurso': 'Alimentador'}
            return render(request, 'MisCosas/error.html', context)
    else:
        return render(request, 'MisCosas/invalid.html', {}, status=404)


@csrf_exempt
def youtube(request, id):
    if request.method == 'POST':
        try:
            form = YoutubeForm(request.POST)
            if form.is_valid():
                ytId = form.cleaned_data['Identificador']
            else:
                ytId = request.POST['Id']
            url = 'https://www.youtube.com/feeds/videos.xml?channel_id=' + ytId
            xmlStream = urllib.request.urlopen(url)
            ytparser.Parser.parse(xmlStream)
            return redirect('youtube', id=ytId)
        except urllib.error.HTTPError:
            return render(request, 'MisCosas/error.html', {}, status=404)
    elif request.method == 'GET':
        try:
            alimentador = Alimentador.objects.get(alimentadorId=id)
            videos = Video.objects.filter(alimentador=alimentador) # obtiene los videos del canal seleccionado
            votos = Voto.objects.filter(usuario=request.user.username, item='video')
            estiloId = get_estilo(request)
            context = {'videos': videos, 'votos': votos, 'alimentador': alimentador, 'estiloId': estiloId}
            return render(request, 'MisCosas/youtube.html', context, status=200)
        except Alimentador.DoesNotExist:
            return render(request, 'MisCosas/error.html', {}, status=404)


def xml_youtube(request, id):
    if request.method == 'GET':
        items = Video.objects.filter(alimentador=Alimentador.objects.get(alimentadorId=id))
        context = {'items': items}
        return render(request, 'MisCosas/youtube.xml', context, content_type='text/xml')
    else:
        return render(request, 'MisCosas/invalid.html', {}, status=404)



def json_youtube(request, id):
    if request.method == 'GET':
        items = Video.objects.filter(alimentador=Alimentador.objects.get(alimentadorId=id))
        context = {'items': items}
        return render(request, 'MisCosas/youtube.json', context, content_type='application/json')
    else:
        return render(request, 'MisCosas/invalid.html', {}, status=404)



@csrf_exempt
def video(request, id): # con GET devuelve la página de un item, con POST, añade un comentario al item
    try:
        v = Video.objects.get(videoId=id)
        if request.method == 'GET':
            comentarios = Comentario.objects.filter(video=v)
            alimentador = v.alimentador
            estiloId = get_estilo(request)
            context = {'video': v, 'comentarios': comentarios, 'alimentador': alimentador, 'form': ComentarioForm, 'estiloId': estiloId}
            return render(request, 'MisCosas/contentVideo.html', context)
        else:
            form = ComentarioForm(request.POST)
            if form.is_valid():
                comentario = Comentario(item='video', titulo=form.cleaned_data['titulo'], cuerpo=form.cleaned_data['cuerpo'],
                    video=v, usuario=request.user.username)
                if request.FILES:
                    comentario.miniatura = request.FILES['miniatura']
                comentario.save()
            return redirect('video', id=id)
    except Video.DoesNotExist: # cuando el vídeo no existe, devuelve un error
        context = {}
        return render(request, 'MisCosas/error.html', context, status=404)



@csrf_exempt
def subreddit(request, id):
    if request.method == 'POST':
        try:
            form = SubredditForm(request.POST)
            if form.is_valid():
                name = form.cleaned_data['Nombre']
            else:
                name = request.POST['Id']
            rdId = name + '.rss'
            url = 'https://www.reddit.com/r/' + rdId
            xmlStream = urllib.request.urlopen(url)
            rdparser.Parser.parse(xmlStream)
            return redirect('subreddit', id=name)
        except urllib.error.HTTPError:
            return render(request, 'MisCosas/error.html', {}, status=404)
    elif request.method == 'GET':
        try:
            alimentador = Alimentador.objects.get(alimentadorId=id)
            noticias = Noticia.objects.filter(alimentador=alimentador) # obtiene los videos del canal seleccionado
            votos = Voto.objects.filter(usuario=request.user.username)
            estiloId = get_estilo(request)
            context = {'noticias': noticias, 'votos': votos, 'alimentador': alimentador, 'estiloId': estiloId}
            return render(request, 'MisCosas/subreddit.html', context, status=200)
        except Alimentador.DoesNotExist:
            return render(request, 'MisCosas/error.html', {}, status=404)



def xml_subreddit(request, id):
    if request.method == 'GET':
        items = Noticia.objects.filter(alimentador=Alimentador.objects.get(alimentadorId=id))
        context = {'items': items}
        return render(request, 'MisCosas/subreddit.xml', context, content_type='text/xml')
    else:
        return render(request, 'MisCosas/invalid.html', {}, status=404)



def json_subreddit(request, id):
    if request.method == 'GET':
        items = Noticia.objects.filter(alimentador=Alimentador.objects.get(alimentadorId=id))
        context = {'items': items}
        return render(request, 'MisCosas/subreddit.json', context, content_type='application/json')
    else:
        return render(request, 'MisCosas/invalid.html', {}, status=404)




@csrf_exempt
def noticia(request, id):
    try:
        n = Noticia.objects.get(noticiaId=id)
        if request.method == 'GET':
            comentarios = Comentario.objects.filter(noticia=n)
            alimentador = n.alimentador
            estiloId = get_estilo(request)
            context = {'new': n, 'comentarios': comentarios, 'alimentador': alimentador, 'form': ComentarioForm, 'estiloId': estiloId}
            return render(request, 'MisCosas/contentNoticia.html', context)
        else:
            form = ComentarioForm(request.POST)
            if form.is_valid():
                comentario = Comentario(item='noticia', titulo=form.cleaned_data['titulo'], cuerpo=form.cleaned_data['cuerpo'],
                    noticia=n, usuario=request.user.username)
                if request.FILES:
                    comentario.miniatura = request.FILES['miniatura']
                comentario.save()
            return redirect('noticia', id=id)
    except Video.DoesNotExist: # cuando la noticia no existe, devuelve un error
        context = {}
        return render(request, 'MisCosas/error.html', context, status=404)



def voto(request, id):
    if request.method == 'POST':
        if request.POST['item'] == 'video':
            v = Video.objects.get(videoId=id)
            try:
                voto = Voto.objects.get(video=v, usuario=request.user.username)
                if request.POST['tipo'] == 'like' and voto.negativo: # si ya se había votado como negativo y ahora como positivo, se cambia el voto a positivo
                    voto.positivo = True
                    voto.negativo = False
                elif request.POST['tipo'] == 'dislike' and voto.positivo:
                    voto.positivo = False
                    voto.negativo = True
            except Voto.DoesNotExist:
                if request.POST['tipo'] == 'like':
                    voto = Voto(item='video', video=v, alimentador=v.alimentador, usuario=request.user.username, positivo=True, negativo=False)
                else:
                    voto = Voto(item='video', video=v, alimentador=v.alimentador, usuario=request.user.username, positivo=False, negativo=True)
            voto.save()
            v.puntuacion = Voto.objects.filter(video=v, positivo=True).count() - Voto.objects.filter(video=v, negativo=True).count()
            v.save()
            a = v.alimentador
            a.puntuacion = Voto.objects.filter(alimentador=a, positivo=True).count() - Voto.objects.filter(alimentador=a, negativo=True).count()
            a.save()
            try:
                i = Item.objects.get(itemId=v.videoId)
                i.puntuacion = v.puntuacion
                i.positivos = Voto.objects.filter(video=v, positivo=True).count()
                i.negativos = Voto.objects.filter(video=v, negativo=True).count()
            except Item.DoesNotExist:
                i = Item(item='video', video=v, titulo=v.titulo,
                        url='http://localhost:8000/MisCosas/video/' + v.videoId,
                        puntuacion = v.puntuacion,
                        positivos = Voto.objects.filter(video=v, positivo=True).count(),
                        negativos = Voto.objects.filter(video=v, negativo=True).count(),
                        itemId = v.videoId)
            i.save()
        else:
            n = Noticia.objects.get(noticiaId=id)
            try:
                voto = Voto.objects.get(noticia=n, usuario=request.user.username)
                if request.POST['tipo'] == 'like' and voto.negativo: # si ya se había votado como negativo y ahora como positivo, se cambia el voto a positivo
                    voto.positivo = True
                    voto.negativo = False
                elif request.POST['tipo'] == 'dislike' and voto.positivo:
                    voto.positivo = False
                    voto.negativo = True
            except Voto.DoesNotExist:
                if request.POST['tipo'] == 'like':
                    voto = Voto(item='noticia', noticia=n, alimentador=n.alimentador, usuario=request.user.username, positivo=True, negativo=False)
                else:
                    voto = Voto(item='noticia', noticia=n, alimentador=n.alimentador, usuario=request.user.username, positivo=False, negativo=True)
            voto.save()
            n.puntuacion = Voto.objects.filter(noticia=n, positivo=True).count() - Voto.objects.filter(noticia=n, negativo=True).count()
            n.save()
            a = n.alimentador
            a.puntuacion = Voto.objects.filter(alimentador=a, positivo=True).count() - Voto.objects.filter(alimentador=a, negativo=True).count()
            a.save()
            try:
                i = Item.objects.get(itemId=n.noticiaId)
                i.puntuacion = n.puntuacion
                i.positivos = Voto.objects.filter(noticia=n, positivo=True).count()
                i.negativos = Voto.objects.filter(noticia=n, negativo=True).count()
            except Item.DoesNotExist:
                i = Item(item='noticia', noticia=n, titulo=n.titulo,
                        url='http://localhost:8000/MisCosas/noticia/' + n.noticiaId,
                        puntuacion = n.puntuacion,
                        positivos = Voto.objects.filter(noticia=n, positivo=True).count(),
                        negativos = Voto.objects.filter(noticia=n, negativo=True).count(),
                        itemId = n.noticiaId)
            i.save()
        if request.POST['origen'] == 'index':
            return redirect('index')
        else:
            return redirect(request.POST['origen'], id)
    else:
        return render(request, 'MisCosas/invalid.html', {}, status=404)



def usuario(request, id):
    if request.method == 'GET':
        usuario = User.objects.get(username=id)
        u = Usuario.objects.get(usuario=usuario)
        votes = Voto.objects.filter(usuario=id)
        comments = Comentario.objects.filter(usuario=id)
        form = EstiloForm()
        estiloId = get_estilo(request)
        context = {'usuario': u, 'votes': votes, 'comments': comments, 'form': form, 'estiloId': estiloId}
        return render(request, 'MisCosas/usuario.html', context)
    else:
        return render(request, 'MisCosas/invalid.html', {}, status=404)



def usuarios(request):
    if request.method == 'GET':
        for u in Usuario.objects.all(): # actualiza el número de items votados y comentados por el usuario antes
        # de mostrar la info de la página de usuarios
            u.votos = Voto.objects.filter(usuario=u.usuario).count()
            u.comentarios = Comentario.objects.filter(usuario=u.usuario).count()
            u.save()
        usuarios = Usuario.objects.all()
        estiloId = get_estilo(request)
        context = {'usuarios': usuarios, 'estiloId': estiloId}
        return render(request, 'MisCosas/usuarios.html', context, status=200)
    else:
        return render(request, 'MisCosas/invalid.html', {}, status=404)



def xml_usuarios(request):
    if request.method == 'GET':
        usuarios = Usuario.objects.all()
        context = {'users': usuarios}
        return render(request, 'MisCosas/usuarios.xml', context, content_type='text/xml')
    else:
        return render(request, 'MisCosas/invalid.html', {}, status=404)



def json_usuarios(request):
    if request.method == 'GET':
        usuarios = Usuario.objects.all()
        context = {'users': usuarios}
        return render(request, 'MisCosas/usuarios.json', context, content_type='application/json')
    else:
        return render(request, 'MisCosas/invalid.html', {}, status=404)



@csrf_exempt
def modify(request, id):
    if request.method == 'POST':
        if request.FILES:
            avatar = request.FILES['foto']
            usuario = User.objects.get(username=id)
            u = Usuario.objects.get(usuario=usuario)
            u.foto = avatar
            u.save()
            return redirect('usuario', id)
        else:
            return render(request, 'MisCosas/invalid.html', {}, status=404)
    else:
        return render(request, 'MisCosas/invalid.html', {}, status=404)



def estilo(request, id):
    if request.method == 'POST':
        usuario = User.objects.get(username=id)
        u = Usuario.objects.get(usuario=usuario)
        try:
            estilo = Estilo.objects.get(usuario=u)
            form = EstiloForm(request.POST)
            if form.is_valid():
                estilo.formato = form.cleaned_data['formato']
                estilo.tamano = form.cleaned_data['tamano']
                estilo.estiloId = form.cleaned_data['formato'] + "_" + form.cleaned_data['tamano']
                print(estilo.estiloId)
        except Estilo.DoesNotExist:
            form = EstiloForm(request.POST)
            if form.is_valid():
                estilo = Estilo(formato=form.cleaned_data['formato'], tamano=form.cleaned_data['tamano'], usuario=u)
                estilo.estiloId = form.cleaned_data['formato'] + "_" + form.cleaned_data['tamano']
        estilo.save()
        return redirect('usuario', id)
    else:
        return render(request, 'MisCosas/invalid.html', {}, status=404)


def info(request):
    estiloId = get_estilo(request)
    context = {'estiloId': estiloId}
    return render(request, 'MisCosas/info.html', context, status=200)
