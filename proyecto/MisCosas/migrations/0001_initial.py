# Generated by Django 3.0.3 on 2020-06-07 12:48

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion
import django.utils.timezone


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Alimentador',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nombre', models.CharField(max_length=200)),
                ('titulo', models.CharField(max_length=200)),
                ('enlace', models.CharField(max_length=200)),
                ('alimentadorId', models.CharField(max_length=200)),
                ('items', models.IntegerField()),
                ('puntuacion', models.IntegerField(default=0)),
                ('seleccionado', models.BooleanField(default=True)),
            ],
        ),
        migrations.CreateModel(
            name='Noticia',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('titulo', models.CharField(max_length=200)),
                ('url', models.CharField(max_length=200)),
                ('descripcion', models.CharField(max_length=200)),
                ('subreddit', models.CharField(max_length=200)),
                ('noticiaId', models.CharField(max_length=200)),
                ('link', models.CharField(max_length=200)),
                ('puntuacion', models.IntegerField(default=0)),
                ('alimentador', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='MisCosas.Alimentador')),
            ],
        ),
        migrations.CreateModel(
            name='Video',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('titulo', models.CharField(max_length=200)),
                ('url', models.CharField(max_length=200)),
                ('videoId', models.CharField(default='', max_length=200)),
                ('videoEmbed', models.TextField()),
                ('canal', models.CharField(max_length=200)),
                ('link', models.CharField(max_length=200)),
                ('descripcion', models.TextField()),
                ('puntuacion', models.IntegerField(default=0)),
                ('alimentador', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='MisCosas.Alimentador')),
            ],
        ),
        migrations.CreateModel(
            name='Voto',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('item', models.CharField(max_length=10)),
                ('usuario', models.CharField(max_length=20)),
                ('positivo', models.BooleanField(default=False)),
                ('negativo', models.BooleanField(default=False)),
                ('fecha', models.DateTimeField(default=django.utils.timezone.now, verbose_name='publicado')),
                ('alimentador', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='MisCosas.Alimentador')),
                ('noticia', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='MisCosas.Noticia')),
                ('video', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='MisCosas.Video')),
            ],
        ),
        migrations.CreateModel(
            name='Usuario',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('foto', models.ImageField(blank=True, upload_to='usuarios/')),
                ('votos', models.IntegerField(default=0)),
                ('comentarios', models.IntegerField(default=0)),
                ('url', models.CharField(max_length=200)),
                ('usuario', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.CreateModel(
            name='Item',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('item', models.CharField(max_length=200)),
                ('titulo', models.CharField(max_length=200)),
                ('url', models.CharField(max_length=200)),
                ('itemId', models.CharField(max_length=10)),
                ('puntuacion', models.IntegerField(default=0)),
                ('positivos', models.IntegerField(default=0)),
                ('negativos', models.IntegerField(default=0)),
                ('noticia', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='MisCosas.Noticia')),
                ('video', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='MisCosas.Video')),
            ],
        ),
        migrations.CreateModel(
            name='Estilo',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('formato', models.CharField(max_length=10)),
                ('tamano', models.CharField(max_length=10)),
                ('estiloId', models.CharField(max_length=10)),
                ('usuario', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='MisCosas.Usuario')),
            ],
        ),
        migrations.CreateModel(
            name='Comentario',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('item', models.CharField(max_length=10)),
                ('usuario', models.CharField(max_length=200)),
                ('titulo', models.CharField(max_length=200)),
                ('cuerpo', models.CharField(max_length=256)),
                ('miniatura', models.ImageField(blank=True, upload_to='comentarios/')),
                ('fecha', models.DateTimeField(auto_now_add=True)),
                ('noticia', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='MisCosas.Noticia')),
                ('video', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='MisCosas.Video')),
            ],
        ),
    ]
