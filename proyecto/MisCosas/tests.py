from django.contrib.auth.models import User
from django.test import Client, TestCase, SimpleTestCase
from .models import Alimentador, Video, Noticia, Comentario, Voto, Usuario, Estilo
from . import views

class TestHTTP(TestCase):
    def setUp(self):
        """Ejecutando antes de los test"""
        self.alimentadorId1 = 'UC300utwSVAYOoRLEqmsprfg'
        self.alimentadorId2 = 'UC300utwSVAYOoRLEqmspg'
        self.alimentadorId3 = 'UCUHW94eEFW7hkUMVaZz4eDg'
        self.alimentadorId4 = 'home'
        self.alimentadorId5 = 'memes'
        Alimentador.objects.create(nombre='youtube', titulo='CursosWeb', enlace='https://www.youtube.com/channel/UC300utwSVAYOoRLEqmsprfg',
            alimentadorId='UC300utwSVAYOoRLEqmsprfg', items=15, puntuacion=10, seleccionado=True)
        Alimentador.objects.create(nombre='youtube', titulo='Congreso', enlace='https://www.youtube.com/channel/UC300utwSVAYOoRLEqmspg',
            alimentadorId='UC300utwSVAYOoRLEqmspg', items=15, puntuacion=10, seleccionado=False)
        Alimentador.objects.create(nombre='subreddit', titulo='Home', enlace='https://www.reddit.com/r/home',
            alimentadorId='home', items=15, puntuacion=10, seleccionado=False)
        self.videoId = 'olkevfYQSYA'
        Video.objects.create(alimentador=Alimentador.objects.get(titulo='Congreso'), titulo='Comisión para la Reconstrucción Económica y Social (26/05/2020)',
            url='https://www.youtube.com/watch?v=olkevfYQSYA', videoId='olkevfYQSYA', videoEmbed='http://www.youtube.com/embed/olkevfYQSYA',
            canal='Congreso de los Diputados - Canal Parlamento', link='https://www.youtube.com/channel/UCT3tvU3bVxOa3ZiVD-B7h9g', puntuacion=0)
        self.noticiaId = 't3_gqh1t2'
        self.noticiaId2 = 't3_gv10n7'
        Noticia.objects.create(alimentador=Alimentador.objects.get(titulo='Home'), titulo='Family house size - way too big',
            url='https://www.reddit.com/r/Home/comments/gqh1t2/family_house_size_way_too_big/', subreddit='Home', noticiaId='t3_gqh1t2',
            link='https://www.reddit.com/r/home', puntuacion=0)
        self.usuarioId = 'manuela'
        User.objects.create_user(self.usuarioId, "", "")
        Usuario.objects.create(usuario=User.objects.get(username=self.usuarioId), votos=3, comentarios=9, url='')


    def test_login(self):
        response = self.client.get('/login/')
        self.assertEqual(response.status_code, 404)

    def test_logout(self):
        response = self.client.post('/logout/', {'origen': 'index'})
        self.assertEqual(response.resolver_match.func, views.logout_view)

    def test_registro_get(self):
        response = self.client.get('/registro/')
        self.assertEqual(response.status_code, 200)

    def test_registro_post(self):
        response = self.client.post('/registro/', {'usuario': 'javi', 'contraseña': 'hola', 'email': ''})
        usuario = User.objects.get(username='javi')
        self.assertEqual(usuario.username, 'javi')

    def test_index(self):
        response = self.client.get('/')
        self.assertEqual(response.status_code, 200)

    def test_xml(self):
        response = self.client.get('/format=xml')
        self.assertEqual(response.resolver_match.func, views.xml)

    def test_json(self):
        response = self.client.get('/format=json')
        self.assertEqual(response.resolver_match.func, views.json)

    def test_descarga(self):
        response = self.client.post('/descarga/indexApp.xml')
        self.assertEqual(response.status_code, 404)

    def test_descarga_get(self):
        response = self.client.get('/descarga/indexApp.xml')
        self.assertEqual(response.resolver_match.func, views.descarga)

    def test_descarga_id(self):
        response = self.client.get('/descarga/indexApp.xml/12')
        self.assertEqual(response.resolver_match.func, views.descarga_id)

    def test_alimentadores(self):
        response = self.client.get('/alimentadores')
        self.assertInHTML("<h2>Alimentadores que ha servido MisCosas</h2>", response.content.decode(encoding='UTF-8'))

    def test_alim_xml(self):
        response = self.client.get('/alimentadores/format=xml')
        self.assertEqual(response.resolver_match.func, views.xml_alimentadores)

    def test_alim_json(self):
        response = self.client.get('/alimentadores/format=json')
        self.assertEqual(response.resolver_match.func, views.json_alimentadores)

    def test_eliminar(self):
        response = self.client.post('/eliminar/' + self.alimentadorId1, {'origen': 'youtube'})
        self.assertEqual(response.status_code, 302)

    def test_seleccionar(self):
        response = self.client.post('/seleccionar/' + self.alimentadorId4)
        alim = Alimentador.objects.get(alimentadorId=self.alimentadorId4)
        self.assertEqual(alim.seleccionado, True)

    def test_youtube_post(self):
        response = self.client.post('/youtube/0', {'Identificador': self.alimentadorId3})
        self.assertEqual(response.resolver_match.func, views.youtube)

    def test_youtube_get(self):
        response = self.client.post('/youtube/0', {'Identificador': self.alimentadorId3})
        response = self.client.get('/youtube/' + self.alimentadorId3)
        self.assertEqual(response.resolver_match.func, views.youtube)

    def test_youtube_xml(self):
        response = self.client.get('/youtube/' + self.alimentadorId2 + '/format=xml')
        self.assertEqual(response.resolver_match.func, views.xml_youtube)

    def test_youtube_json(self):
        response = self.client.get('/youtube/' + self.alimentadorId2 + '/format=json')
        self.assertEqual(response.resolver_match.func, views.json_youtube)

    def test_video_post(self):
        response = self.client.post('/video/'+ self.videoId, {'titulo': 'Primer comentario',
                                    'cuerpo': 'Este es el único comentario'})
        comentario = Comentario.objects.get(titulo='Primer comentario')
        self.assertEqual(comentario.cuerpo, 'Este es el único comentario')

    def test_video_get(self):
        response = self.client.get('/video/' + self.videoId)
        self.assertEqual(response.resolver_match.func, views.video)

    def test_subreddit_post(self):
        response = self.client.post('/subreddit/0', {'Nombre': self.alimentadorId5})
        alim = Alimentador.objects.get(alimentadorId=self.alimentadorId5)
        self.assertEqual(alim.titulo, 'Memes the original since 2008')

    def test_subreddit_get(self):
        response = self.client.get('/subreddit/' + self.alimentadorId4)
        self.assertEqual(response.resolver_match.func, views.subreddit)

    def test_subreddit_xml(self):
        response = self.client.get('/subreddit/home/format=xml')
        self.assertEqual(response.resolver_match.func, views.xml_subreddit)

    def test_subreddit_json(self):
        response = self.client.get('/subreddit/home/format=json')
        self.assertEqual(response.resolver_match.func, views.json_subreddit)

    def test_noticia_post(self):
        response = self.client.post('/noticia/'+ self.noticiaId, {'titulo': 'Primer comentario de la noticia',
                                    'cuerpo': 'Este es el único comentario'})
        comentario = Comentario.objects.get(noticia=Noticia.objects.get(noticiaId=self.noticiaId))
        self.assertEqual(comentario.titulo, 'Primer comentario de la noticia')

    def test_noticia_get(self):
        response = self.client.get('/noticia/' + self.noticiaId)
        self.assertEqual(response.resolver_match.func, views.noticia)

    def test_voto_like_func(self):
        response = self.client.post('/voto/' + self.videoId, {'tipo': 'like', 'item': 'video', 'origen': 'index'})
        self.assertEqual(response.resolver_match.func, views.voto)

    def test_voto_like(self):
        response = self.client.post('/voto/' + self.videoId, {'tipo': 'like', 'item': 'video', 'origen': 'index'})
        voto = Voto.objects.get(video=Video.objects.get(videoId=self.videoId))
        self.assertEqual(voto.positivo, True)

    def test_voto_like_code(self):
        response = self.client.post('/voto/' + self.videoId, {'tipo': 'like', 'item': 'video', 'origen': 'index'})
        self.assertEqual(response.status_code, 302)

    def test_voto_dislike_code(self):
        response = self.client.post('/voto/' + self.noticiaId, {'tipo': 'dislike', 'item': 'noticia', 'origen': 'index'})
        self.assertEqual(response.status_code, 302)

    def test_voto_dislike_func(self):
        response = self.client.post('/voto/' + self.noticiaId, {'tipo': 'dislike', 'item': 'noticia', 'origen': 'index'})
        self.assertEqual(response.resolver_match.func, views.voto)

    def test_voto_dislike(self):
        response = self.client.post('/voto/' + self.noticiaId, {'tipo': 'dislike', 'item': 'noticia', 'origen': 'index'})
        voto = Voto.objects.get(noticia=Noticia.objects.get(noticiaId=self.noticiaId))
        self.assertEqual(voto.negativo, True)

    def test_usuarios_code(self):
        response = self.client.get('/usuarios/')
        self.assertEqual(response.status_code, 200)

    def test_usuarios_func(self):
        response = self.client.get('/usuarios/')
        self.assertEqual(response.resolver_match.func, views.usuarios)

    def test_usuarios_html(self):
        response = self.client.get('/usuarios/')
        self.assertInHTML("<h2>Usuarios de MisCosas</h2>", response.content.decode(encoding='UTF-8'))

    def test_usuarios_xml(self):
        response = self.client.get('/usuarios/format=xml')
        self.assertEqual(response.resolver_match.func, views.xml_usuarios)

    def test_usuarios_json(self):
        response = self.client.get('/usuarios/format=json')
        self.assertEqual(response.resolver_match.func, views.json_usuarios)

    def test_usuario_code(self):
        response = self.client.get('/usuario/' + self.usuarioId)
        self.assertEqual(response.status_code, 200)

    def test_usuario_func(self):
        response = self.client.get('/usuario/' + self.usuarioId)
        self.assertEqual(response.resolver_match.func, views.usuario)

    def test_usuario_html(self):
        response = self.client.get('/usuario/' + self.usuarioId)
        self.assertInHTML("<h2>Información del usuario</h2>", response.content.decode(encoding='UTF-8'))

    def test_modify_code(self):
        response = self.client.post('/modify/manuela', {'foto': ''})
        self.assertEqual(response.status_code, 404) # debe dar 404 porque no se le pasa foto

    def test_modify_func(self):
        response = self.client.post('/modify/manuela', {'foto': ''})
        self.assertEqual(response.resolver_match.func, views.modify) # debe dar 404 porque no se le pasa foto

    def test_estilo_code(self):
        response = self.client.post('/estilo/' + self.usuarioId, {'formato': 'Oscuro', 'tamano': 'Normal'})
        self.assertEqual(response.status_code, 302)

    def test_estilo_func(self):
        response = self.client.post('/estilo/' + self.usuarioId, {'formato': 'Oscuro', 'tamano': 'Normal'})
        u = User.objects.get(username=self.usuarioId)
        usuario = Usuario.objects.get(usuario=u)
        estilo = Estilo.objects.get(usuario=usuario)
        self.assertEqual(estilo.formato, 'Oscuro') # debe dar 404 porque no se le pasa foto

    def test_info_code(self):
        response = self.client.get('/informacion')
        self.assertEqual(response.status_code, 200)

    def test_info_func(self):
        response = self.client.get('/informacion')
        self.assertEqual(response.resolver_match.func, views.info)

    def test_info_html(self):
        response = self.client.get('/informacion')
        self.assertInHTML("<h2>Información de MisCosas.</h2>", response.content.decode(encoding='UTF-8'))
