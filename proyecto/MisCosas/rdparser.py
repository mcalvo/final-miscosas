#!/usr/bin/python3

from xml.sax.handler import ContentHandler
from xml.sax import make_parser
from .models import Alimentador, Noticia
import sys
import string
import html
import html2text

import urllib.request

items = 0
class YTHandler(ContentHandler):

    def __init__ (self):
        self.inEntry = False
        self.inContent = False
        self.inFeed = False
        self.inFeedContent = False
        self.content = ""
        self.title = ""
        self.link = ""
        self.subreddit = ""
        self.subredditLink = ""
        self.description = ""
        self.NoticiaId = ""
        self.alimentTitle = ""
        self.alimentId = ""

    def startElement (self, name, attrs):
        # global alimentador
        if name == 'feed':
            self.inFeed = True
        elif name == 'entry':
            self.inEntry = True
            self.inFeedContent = False
            try:
                alimentador = Alimentador.objects.get(titulo=self.alimentTitle)
            except Alimentador.DoesNotExist:
                alimentador = Alimentador(nombre='subreddit', titulo=self.alimentTitle, enlace=self.alimentLink,
                        alimentadorId=self.alimentId, items=0)
                alimentador.save()
        elif self.inEntry:
            if name == 'title' or name == 'id':
                self.inContent = True
            if  name == 'content':
                self.inContent = True

            elif name == 'link':
                self.link = attrs.get('href')
        elif self.inFeed:
            if name == 'id' or name == 'title':
                self.inFeedContent = True
            elif name == 'link' and self.inFeedContent:
                if attrs.get('rel') == 'alternate':
                    self.alimentLink = attrs.get('href')
                    # self.inFeedContent = False

    def endElement (self, name):
        global items
        if name == 'entry':
            self.inEntry = False
            try: # comprueba que el Noticia no esté ya en la base de datos
                n = Noticia.objects.get(titulo=self.title)
            except Noticia.DoesNotExist: # si el vídeo no está en la base de datos, lo añade
                a = Alimentador.objects.get(titulo=self.alimentTitle)
                n = Noticia(alimentador=a, titulo=self.title, url=self.link, subreddit=a.titulo, link=a.enlace, # fecha=self.date,
                    descripcion=self.description, noticiaId=self.NoticiaId)
                n.save()
                a = Alimentador.objects.get(titulo=self.alimentTitle)
                a.items = a.items + 1
                a.save()
        elif self.inFeedContent:
            if name == 'title':
                self.content = self.content.split('/r/')[-1]
                self.alimentTitle = self.content
            elif name == 'id':
                self.content = self.content.split('/r/')[-1]
                self.alimentId = self.content.split('.rss')[0]
            self.content = ""
        elif self.inEntry:
            if name == 'title':
                self.content = self.content.split('/r/')[-1]
                self.title = self.content
            elif name == 'id':
                self.NoticiaId = self.content
            elif name == 'content':
                # convierte quotes a string y convierte el html a texto
                self.description = html.unescape(html2text.html2text(self.content))
                self.description = self.description.split('submitted by')[0]
            self.content = ""
            self.inContent = False

    def characters (self, chars):
        if self.inContent or self.inFeedContent:
            self.content = self.content + chars

# Load parser and driver
Parser = make_parser()
Parser.setContentHandler(YTHandler())

# --- Main prog
if __name__ == "__main__":

    PAGE = """
<!DOCTYPE html>
<html lang="en">
  <body>
    <h1>subreddit contents:</h1>
    <ul>
{Noticias}
    </ul>
  </body>
</html>
"""

    if len(sys.argv)<2:
        print("Usage: python xml-parser-youtube.py <document>")
        print()
        print(" <document>: file name of the document to parse")
        sys.exit(1)

    # Ready, set, go!
    xmlFile = open(sys.argv[1],"r")

    Parser.parse(xmlFile)
    page = PAGE.format(Noticias=Noticias)
    print(page)
