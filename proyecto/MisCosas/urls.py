from django.urls import path
from . import views
from django.conf import settings
from django.conf.urls.static import static

from django.utils.translation import ugettext_lazy as _ #internacionalizacion



urlpatterns = [
    path('', views.index, name='index'),
    path('format=json', views.json, name='json'),
    path('format=xml', views.xml, name='xml'),
    path('alimentadores', views.alimentadores, name='alimentadores'),
    path('alimentadores/format=xml', views.xml_alimentadores, name='xml_alimentadores'),
    path('alimentadores/format=json', views.json_alimentadores, name='json_alimentadores'),
    path('descarga/<str:doc>', views.descarga, name='descarga'),
    path('descarga/<str:doc>/<str:id>', views.descarga_id, name='descarga_id'),
    path('descarga_com/<str:id>/<str:doc>', views.descarga_com, name='descarga_com'),
    path('eliminar/<str:id>', views.eliminar, name='eliminar'),
    path('estilo/<str:id>', views.estilo, name='estilo'),
    path('informacion', views.info, name='info'),
    path('login/', views.login_view, name='login'),
    path('logout/', views.logout_view, name='logout'),
    path('modify/<str:id>', views.modify, name='modyfy'),
    path('noticia/<str:id>', views.noticia, name='noticia'),
    path('registro/', views.registro, name='registro'),
    path('seleccionar/<str:id>', views.seleccionar, name='seleccionar'),
    path('subreddit/<str:id>', views.subreddit, name='subreddit'),
    path('subreddit/<str:id>/format=xml', views.xml_subreddit, name='xml_subreddit'),
    path('subreddit/<str:id>/format=json', views.json_subreddit, name='json_subreddit'),
    path('usuario/<str:id>', views.usuario, name='usuario'),
    path('usuarios/', views.usuarios, name='usuarios'),
    path('usuarios/format=xml', views.xml_usuarios, name='xml_usuarios'),
    path('usuarios/format=json', views.json_usuarios, name='json_usuarios'),
    path('video/<str:id>', views.video, name='video'),
    path('voto/<str:id>', views.voto, name='voto'),
    path('youtube/<str:id>', views.youtube, name='youtube'),
    path('youtube/<str:id>/format=xml', views.xml_youtube, name='xml_youtube'),
    path('youtube/<str:id>/format=json', views.json_youtube, name='json_youtube'),
]+ static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
