from django.db import models
from django.utils import timezone
from django.contrib.auth.models import User

# Create your models here.


class Usuario(models.Model):
    usuario = models.OneToOneField(User, on_delete=models.CASCADE)
    foto = models.ImageField(upload_to='usuarios/', blank=True)
    votos = models.IntegerField(default=0)
    comentarios = models.IntegerField(default=0)
    url = models.CharField(max_length=200)
    def __str__(self):
        return self.usuario.username


class Estilo(models.Model):
    usuario = models.ForeignKey(Usuario, on_delete=models.CASCADE)
    formato = models.CharField(max_length=10)
    tamano = models.CharField(max_length=10)
    estiloId = models.CharField(max_length=10)


class Alimentador(models.Model):
    nombre = models.CharField(max_length=200)
    titulo = models.CharField(max_length=200)
    enlace = models.CharField(max_length=200)
    alimentadorId = models.CharField(max_length=200)
    items = models.IntegerField()
    puntuacion = models.IntegerField(default=0)
    seleccionado = models.BooleanField(default=True)
    def __str__(self):
        return self.titulo

class Video(models.Model):
    alimentador = models.ForeignKey(Alimentador, on_delete=models.CASCADE)
    titulo = models.CharField(max_length=200)
    url = models.CharField(max_length=200)
    videoId = models.CharField(max_length=200, default='')
    videoEmbed = models.TextField(blank=False)
    canal = models.CharField(max_length=200)
    link = models.CharField(max_length=200)
    descripcion = models.TextField(blank=False)
    puntuacion = models.IntegerField(default=0)
    def __str__(self):
        return self.titulo

class Noticia(models.Model):
    alimentador = models.ForeignKey(Alimentador, on_delete=models.CASCADE)
    titulo = models.CharField(max_length=200)
    url = models.CharField(max_length=200)
    descripcion = models.CharField(max_length=200)
    subreddit = models.CharField(max_length=200)
    noticiaId = models.CharField(max_length=200)
    link = models.CharField(max_length=200)
    puntuacion = models.IntegerField(default=0)
    def __str__(self):
        return self.titulo

class Item(models.Model):
    item = models.CharField(max_length=200)
    noticia = models.ForeignKey(Noticia, on_delete=models.CASCADE, null=True)
    video = models.ForeignKey(Video, on_delete=models.CASCADE, null=True)
    titulo = models.CharField(max_length=200)
    url = models.CharField(max_length=200)
    itemId = models.CharField(max_length=10)
    puntuacion = models.IntegerField(default=0)
    positivos = models.IntegerField(default=0)
    negativos = models.IntegerField(default=0)
    def __str__(self):
        return self.titulo

class Voto(models.Model):
    item = models.CharField(max_length=10)
    noticia = models.ForeignKey(Noticia, on_delete=models.CASCADE, null=True)
    video = models.ForeignKey(Video, on_delete=models.CASCADE, null=True)
    alimentador = models.ForeignKey(Alimentador, on_delete=models.CASCADE)
    usuario = models.CharField(max_length=20)
    positivo = models.BooleanField(default=False)
    negativo = models.BooleanField(default=False)
    fecha = models.DateTimeField('publicado', default=timezone.now)
    def __str__(self):
        return self.usuario

class Comentario(models.Model):
    item = models.CharField(max_length=10)
    noticia = models.ForeignKey(Noticia, on_delete=models.CASCADE, null=True)
    video = models.ForeignKey(Video, on_delete=models.CASCADE, null=True)
    usuario = models.CharField(max_length=200)
    titulo = models.CharField(max_length=200)
    cuerpo = models.CharField(max_length=256)
    miniatura = models.ImageField(upload_to='comentarios/', blank=True)
    fecha = models.DateTimeField(auto_now_add=True)
    def __str__(self):
        return self.titulo
