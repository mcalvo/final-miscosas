from django import forms

from .models import Estilo

format = [('Oscuro', 'Oscuro'), ('Ligero', 'Ligero')]

size = [('Reducido', 'Reducido'), ('Normal', 'Normal'), ('Ampliado', 'Ampliado')]


class ComentarioForm(forms.Form):
    titulo = forms.CharField(max_length=200)
    cuerpo = forms.CharField(max_length=200)
    miniatura = forms.ImageField(required=False)

class FotoUsuarioForm(forms.Form):
    foto = forms.ImageField(required=False)

class EstiloForm(forms.Form):
    formato = forms.ChoiceField(choices=format)
    tamano = forms.ChoiceField(choices=size)

class YoutubeForm(forms.Form):
    Identificador = forms.CharField(max_length=50)

class SubredditForm(forms.Form):
    Nombre = forms.CharField(max_length=50)
