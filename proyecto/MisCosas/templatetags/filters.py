from django import template
from MisCosas.models import Voto

register = template.Library()

@register.filter(name='vote_video')
def vote_video(video, username):
    return Voto.objects.filter(usuario=username, video=video)

@register.filter(name='vote_new')
def vote_new(new, username):
    return Voto.objects.filter(usuario=username, noticia=new)
