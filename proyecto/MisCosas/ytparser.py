#!/usr/bin/python3

from xml.sax.handler import ContentHandler
from xml.sax import make_parser
from .models import Alimentador, Video
import sys
import string

import urllib.request

# alimentador = ''
items = 0
class YTHandler(ContentHandler):

    def __init__ (self):
        self.inEntry = False
        self.inContent = False
        self.inFeed = False
        self.inFeedContent = False
        self.content = ""
        self.title = ""
        self.link = ""
        self.channel = ""
        self.channelLink = ""
        self.description = ""
        self.videoId = ""
        self.videoEmbed = ""
        self.alimentTitle = ""
        self.alimentId = ""

    def startElement (self, name, attrs):
        if name == 'feed':
            self.inFeed = True
        elif name == 'entry':
            self.inEntry = True
            try:
                alimentador = Alimentador.objects.get(titulo=self.alimentTitle)
            except Alimentador.DoesNotExist:
                alimentador = Alimentador(nombre='youtube', titulo=self.alimentTitle, enlace=self.alimentLink,
                        alimentadorId=self.alimentId, items=0)
                alimentador.save()
        elif self.inEntry:
            if name == 'title' or name == 'yt:videoId' or name == 'name' or name == 'uri' \
                or name == 'published' or name == 'media:description':
                self.inContent = True
            elif name == 'link':
                self.link = attrs.get('href')
        elif self.inFeed:
            if name == 'title' or name == 'yt:channelId':
                self.inFeedContent = True
            elif name == 'link' and self.inFeedContent:
                self.alimentLink = attrs.get('href')
                self.inFeedContent = False

    def endElement (self, name):
        global items
        if name == 'entry':
            self.inEntry = False
            try:
                v = Video.objects.get(titulo=self.title)
            except Video.DoesNotExist: # si el vídeo no está en la base de datos, lo añade
                a = Alimentador.objects.get(titulo=self.alimentTitle)
                v = Video(alimentador=a, titulo=self.title, url=self.link, videoEmbed=self.videoEmbed,
                    canal=self.channel, link=self.channelLink, # fecha=self.date,
                    descripcion=self.description, videoId=self.videoId)
                v.save()
                items = items + 1
        elif name == 'feed':
            if items != 0:
                a = Alimentador.objects.get(titulo=self.alimentTitle)
                a.items = items
                a.save()
                items = 0
        elif self.inFeedContent:
            if name == 'title':
                self.alimentTitle = self.content.split('\n')[-1]
            elif name == 'yt:channelId':
                self.alimentId = self.content
            self.content = ""
        elif self.inEntry:
            if name == 'title':
                self.title = self.content
            elif name == 'yt:videoId':
                self.videoId = self.content
                self.videoEmbed = 'http://www.youtube.com/embed/' + self.videoId
            elif name == 'name':
                self.channel = self.content
            elif name == 'uri':
                self.channelLink = self.content
            elif name == 'media:description':
                self.description = self.content

            self.content = ""
            self.inContent = False

    def characters (self, chars):
        if self.inContent or self.inFeedContent:
            self.content = self.content + chars

# Load parser and driver
Parser = make_parser()
Parser.setContentHandler(YTHandler())

# --- Main prog
if __name__ == "__main__":

    PAGE = """
<!DOCTYPE html>
<html lang="en">
  <body>
    <h1>Channel contents:</h1>
    <ul>
{videos}
    </ul>
  </body>
</html>
"""

    if len(sys.argv)<2:
        print("Usage: python xml-parser-youtube.py <document>")
        print()
        print(" <document>: file name of the document to parse")
        sys.exit(1)

    # Ready, set, go!
    xmlFile = open(sys.argv[1],"r")

    Parser.parse(xmlFile)
    page = PAGE.format(videos=videos)
    print(page)
